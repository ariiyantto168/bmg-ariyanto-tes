@include('contents.partials.header')
        <div class="page-body">
          <div class="container-fluid">
            <div class="page-title">
              <div class="row">
                <div class="col-6">
                  <h3>
                     welcome {{ $data->name }}</h3>
                </div>
                <div class="col-6">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                    <li class="breadcrumb-item">Dashboard</li>
                    <li class="breadcrumb-item active">{{ $data->name }}</li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
          <!-- Container-fluid starts-->
          @include('contents.allmessage')
          <div class="container-fluid">
            <div class="row size-column">
              <div class="col-xl-7 box-col-12 xl-100">
                <div class="row dash-chart">
                  <div class="col-xl-6 box-col-6 col-md-6">
                    <div class="card o-hidden">
                      <div class="card-header card-no-border">
                        <div class="card-header-right">
                          <ul class="list-unstyled card-option">
                            <li><i class="fa fa-spin fa-cog"></i></li>
                            <li><i class="view-html fa fa-code"></i></li>
                            <li><i class="icofont icofont-maximize full-card"></i></li>
                            <li><i class="icofont icofont-minus minimize-card"></i></li>
                            <li><i class="icofont icofont-refresh reload-card"></i></li>
                            <li><i class="icofont icofont-error close-card"></i></li>
                          </ul>
                        </div>
                        <div class="media">
                          <div class="media-body">
                          <p class="f-w-500 font-roboto">Your Balance
                            <span class="badge pill-badge-primary ms-3"
                            data-bs-toggle="modal" 
                            data-original-title="test"
                            data-bs-target="#tambah_modal"
                            >Top Up</span>
                            <span class="badge pill-badge-primary ms-3">Transaksi</span></p>
                            <h4 class="f-w-500 mb-0 f-20"><span class="counter">{{number_format($data->balance, 2,".")}}</span></h4>
                          </div>
                        </div>
                      </div>
                      <div class="card-body p-0">
                        <div class="media">
                          <div class="media-body">
                            <div class="profit-card">
                              <div id="spaline-chart"></div>
                            </div>
                          </div>
                        </div>
                        <div class="code-box-copy">
                          <button class="code-box-copy__btn btn-clipboard" data-clipboard-target="#example-head" title="Copy"><i class="icofont icofont-copy-alt"></i></button>
                            <pre><code class="language-html" id="example-head">
                                </code>
                            </pre>
                        </div>
                      </div>
                    </div>
                  </div>
                 <!-- close div -->
                 <section class="content">
            <div class="container-fluid">
                <div class="row">
                <div class="col-12">
                    <!-- /.card -->
                    <div class="card">
                    
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                          <th>No</th>
                          <th>Nominal</th>
                          <th>Keterangan</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($riwayat as $idx => $riwayat)
                        <tr>
                          <td>{{$idx+1}}</td>
                          <td>Rp. {{number_format($riwayat->nominal, 2,".")}}</td>
                          <td>{{$riwayat->keterangan}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
        </section>
                 <div class="container-fluid">
                    <div class="row second-chart-list third-news-update">
                    <div class="col-sm-12">

                    <div class="modal fade" id="tambah_modal" tabindex="-1" role="dialog" aria-labelledby="tambah_modalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <form class="modal-content" method="post" action="{{ url('topup') }}" enctype="multipart/form-data">
                            @csrf
                                <div class="modal-header">
                                    <h5 class="modal-title" id="tambah_modalLabel">Form Top Up</h5>
                                    <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body py-4">
                                    <div class="form-group">
                                        <label class="col-form-label">Nominal<span class="text-danger"><sup>*</sup></span></label>
                                        <input class="form-control" type="number" required name="balance"   placeholder="Nominal" />
                                    </div>
                                </div>
                                <div class="modal-body py-4">
                                  <div class="form-group">
                                      <select class="form-select digits" name="tipe" id="exampleFormControlSelect9">
                                          <option value="transaksi">Transaksi</option>
                                          <option value="topup">Top Up</option>    
                                      </select>
                                    </div>
                                  </div>
                                <div class="modal-footer">
                                    <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Close</button>
                                    <button class="btn btn-secondary" type="submit">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
             
              
              
          
    </div>
</div>
                </div>
              </div>
            </div>
          </div>
          <!-- Container-fluid Ends-->
        </div>
@include('contents.partials.detail')