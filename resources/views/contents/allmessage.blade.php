@if(session('status_error'))
<div class="alert alert-secondary dark alert-dismissible fade show" role="alert"><strong></strong> 
    <button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
  {{session('status_error')}}
</div>
@elseif(session('status_success'))
<div class="alert alert-success dark alert-dismissible fade show" role="alert" aria-hidden="true"><strong></strong>
    <button class="btn-close" id="bootstrap-notify-gen-btn" type="button" data-bs-dismiss="alert" aria-label="Close">×</button>
    {{session('status_success')}}
</div>
@endif

