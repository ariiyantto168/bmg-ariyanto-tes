@include('contents.partials.header')
  <div class="page-body">
    <div class="container-fluid">
      <div class="page-title">
        <div class="row">
          <div class="col-6">
           <h3>
              Kelas
            </h3>
          </div>
                <div class="col-6">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                    <li class="breadcrumb-item">Dashboard</li>
                    <li class="breadcrumb-item active">Kelas</li>
                  </ol>
                </div>
              </div>
            </div>
        </div>
          <!-- Container-fluid starts-->
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                <div class="col-12">
                    <!-- /.card -->
                    <div class="card">
                    <div class="card-header pull">
                        <h4 class="card-title"> 
                        <a class="btn btn-primary pull-right"
                          >
                          Tambah
                        </a>
                        </h4>
                    </div>
                    
                    <!-- /.card-header -->
                    <div class="card-body">
                        @include('contents.allmessage')
                        <table id="example4" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                          <th>No</th>
                          <th>Nama Kelas</th>
                          <th>Status</th>
                          <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                       
                        </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
        </section>
          <!-- Container-fluid Ends-->
        </div>
@include('contents.partials.detail')