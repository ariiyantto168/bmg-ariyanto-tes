 <!-- latest jquery-->
 <script src="{{asset('cuba/assets/js/jquery-3.5.1.min.js')}}"></script>
    <!-- Bootstrap js-->
    <script src="{{asset('cuba/assets/js/bootstrap/bootstrap.bundle.min.js')}}"></script>
    <!-- feather icon js-->
    <script src="{{asset('cuba/assets/js/icons/feather-icon/feather.min.js')}}"></script>
    <script src="{{asset('cuba/assets/js/icons/feather-icon/feather-icon.js')}}"></script>
    <!-- scrollbar js-->
    <script src="{{asset('cuba/assets/js/scrollbar/simplebar.js')}}"></script>
    <script src="{{asset('cuba/assets/js/scrollbar/custom.js')}}"></script>
    <!-- Sidebar jquery-->
    <script src="{{asset('cuba/assets/js/config.js')}}"></script>
    <!-- Plugins JS start-->
    <script src="{{asset('cuba/assets/js/sidebar-menu.js')}}"></script>
    <script src="{{asset('cuba/assets/js/chart/chartist/chartist.js')}}"></script>
    <script src="{{asset('cuba/assets/js/chart/chartist/chartist-plugin-tooltip.js')}}"></script>
    
    <script src="{{asset('cuba/assets/js/prism/prism.min.js')}}"></script>
    <script src="{{asset('cuba/assets/js/clipboard/clipboard.min.js')}}"></script>
    <script src="{{asset('cuba/assets/js/counter/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('cuba/assets/js/counter/jquery.counterup.min.js')}}"></script>
    <script src="{{asset('cuba/assets/js/counter/counter-custom.js')}}"></script>
    <script src="{{asset('cuba/assets/js/custom-card/custom-card.js')}}"></script>
    <script src="{{asset('cuba/assets/js/owlcarousel/owl.carousel.js')}}"></script>
    <script src="{{asset('cuba/assets/js/tooltip-init.js')}}"></script>
    <!-- Plugins JS Ends-->
    <!-- Theme js-->
    <script src="{{asset('cuba/assets/js/script.js')}}"></script>
    <!-- DataTables  & Plugins -->
<script src="{{asset('cuba/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('cuba/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('cuba/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('cuba/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{asset('cuba/plugins/datatables-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('cuba/plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('cuba/plugins/jszip/jszip.min.js')}}"></script>
<script src="{{asset('cuba/plugins/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{asset('cuba/plugins/pdfmake/vfs_fonts.js')}}"></script>
<script src="{{asset('cuba/plugins/datatables-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('cuba/plugins/datatables-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{asset('cuba/plugins/datatables-buttons/js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('cuba/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- Summernote -->
<script src="{{asset('cuba/plugins/summernote/summernote-bs4.min.js')}}"></script>

<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, 
      "lengthChange": false, 
      "autoWidth": false,
      "searching": true,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

    $("#example2").DataTable({
      "responsive": true, 
      "lengthChange": false, 
      "autoWidth": false,
      "searching": true,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example2_wrapper .col-md-6:eq(0)');

    $("#example3").DataTable({
      "responsive": true, 
      "lengthChange": false, 
      "autoWidth": false,
      "searching": true,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example3_wrapper .col-md-6:eq(0)');

    $("#example4").DataTable({
      "responsive": true, 
      "lengthChange": true, 
      "autoWidth": true,
      "searching": true,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example4_wrapper .col-md-6:eq(0)');

    $("#example5").DataTable({
      "responsive": true, 
      "lengthChange": false, 
      "autoWidth": false,
      "searching": true,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example4_wrapper .col-md-6:eq(0)');

    $('#example22').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });

    $('.summernote').summernote()
  });
</script>
