<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Topup;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

class Profile extends Controller
{
    public function riwayat()
    {
        return view('contents.profile.riwayat');
    }

    public function home()
    {
        $user = User::where('id',Session::get('id'))->first();
       
        if (empty($user)) {
            return redirect('/')->with('status_error','silahkan login');
        }

        $contents = [
            'riwayat'   => Topup::get(),
            'data'      => $user
        ];

        return view('contents.home',$contents);
    }

    public function top(Request $request) 
    {
        $data = $request->all();

        if (empty($data['balance'])) {
            return redirect('home')->with('status_error','masukan balance nominal kamu');
        }

        $data = User::topup($data);
        return redirect('home')->with('status_success','success top up');
    }
}
