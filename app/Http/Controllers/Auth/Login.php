<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Foundation\Http\FormRequest;



class Login extends Controller
{
    public function login()
    {
        return view('contents.auth.login');
    }

    public function register()
    {
        return view('contents.auth.register');
    }

    public function add_user(Request $request)
    {
        $data = $request->all();

        $request->validate([
            'name' => 'required',
            'username' => 'required',
            'email'    => 'required|email|unique:users,email',
            'password'    => 'required',
        ]);

        if (empty($data['name'] || $data['username'] || $data['email'] || $data['password'])) {
            return redirect()->back()->with('status_error', 'data tidak boleh kosong atau data sudah ada ');
        }

        $add_user = User::add_user($data);

        return redirect('/')->with('status_success','silahkan Login');


    }

    public function login_post(Request $request)
    {
        $credentials = $request->only('username', 'password');
        
        if (Auth::attempt($credentials)) {
            $datauser = User::where('username',$request->username)->first();
            $alldata = [
                'id' => $datauser->id,
                'name' => $datauser->name,
                'email' => $datauser->email,
                'username' => $datauser->username
            ];
            
            Session::put('id',$alldata['id']);
            Session::put('name',$alldata['name']);
            Session::put('email',$alldata['email']);
            Session::put('username',$alldata['username']);
            Session::put('/',TRUE);

            return redirect('home')->with('status_success','welcome to pages');
        }else {
            return redirect('/')->with('status_error','username atau password salah');
        }
    
    }
}
