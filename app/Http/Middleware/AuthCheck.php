<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\User;




class AuthCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next)
    {
        $user = User::where('id',Session::get('id'))->first();
        // if(!Session::get('/')){
        //     return redirect('/')->with('alert','Kamu harus login dulu');
        // }
        // if (empty($user)) {
        //     return redirect('/')->with('status_warning', 'Please Login!');
        // }
        return $next($request);
   

    }
}
