<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Models\Topup;
use DB;


class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    protected $table = "users";
   
    protected $fillable = [
        'name',
        'username',
        'email',
        'password',
        'balance'
    ];

    public static function add_user($data) 
    {
        return User::create([
            'name'  => $data['name'],
            'username'  => $data['username'],
            'email'     => $data['email'],
            'password'  => Hash::make($data['password']),
            'balance'   => '0'
            
        ]);    
    }

    public static function topup($data)
    {
        $user = User::where('id',Session::get('id'))->first();

        $topup = Topup::create([
            'user_id'       => Session::get('id'),
            'nominal'       => $data['balance'],
            'tipe'          => $data['tipe'],
            'datetime'      => date('Y-m-d H:i:s'),
            'keterangan'    => 'Top up berhasi sebesar' . "Rp " . number_format($data['balance'], 2, ",", ".")
        ]);

        
        return $user->update(['balance' => $user['balance'] + $data['balance']]);

        

    



    }

}
