<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\Login;
use App\Http\Controllers\Account\Profile;
use App\Http\Middleware\AuthCheck;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('login', [HomeController::class, 'login']);

Route::controller(Login::class)->group(function () {
    Route::get('/', 'login');
    Route::post('/login', 'login_post');
    Route::get('/register', 'register');
    Route::post('/add/register', 'add_user');
});


Route::middleware([AuthCheck::class])->group(function () {
    Route::controller(Profile::class)->group(function () {
        Route::get('/profile', 'profile');
        Route::get('/riwayat', 'riwayat');
        Route::get('/home', 'home');
        Route::post('/topup', 'top');
    });    
});